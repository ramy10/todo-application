#!/bin/bash

curl -X POST http://127.0.0.1:5000/item/create -d '{"SN": "1", "Disc": "Sport", "Time": "1 PM"}' -H 'Content-Type: application/json'
curl -X POST http://127.0.0.1:5000/item/create -d '{"SN": "2", "Disc": "Work", "Time": "8 PM"}' -H 'Content-Type: application/json'
curl -X POST http://127.0.0.1:5000/item/create -d '{"SN": "3", "Disc": "Travel", "Time": "5 PM"}' -H 'Content-Type: application/json'
