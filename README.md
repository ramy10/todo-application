## A web service for a TODO application

The main goal is to construct ReST APIs to achieve the following requirements:

1. To create a new item in the list.

2. To update an existing item.

3. To delete an item.

4. To get a particular item.

5. To get all items from the list.

## Tools and Technologies

Project is created with:

* Python 3.8.5

* Flask 1.1.2

* JSON

## Starting the application

To run this project along with its test cases, please open the terminal and run the following commands:

1. ./StartWebService.sh     (Start the web service)

2. ./Add3Items.sh           (Create 3 items)

3. ./Update2ndItem.sh       (Update 2nd item in the list)

4. ./Delete3rdItem.sh       (Delete the 3rd item)

5. ./Get1stItem.sh          (Get the 1st item in the list)

6. ./GetAll.sh              (Get all items in the list)
