from flask import Flask, request, Response
import json

app = Flask(__name__)

itemsDict = []                                                                  # A list of dictionaries where all items will be stored (No database)


#1: Create a new item in the list.
@app.route('/item/create', methods=['POST'])
def add_item():
    requested_data = request.get_json()                                         # Get the item from POST payload
    SN = requested_data['SN']                                                   # The serial number
    Disc = requested_data['Disc']                                               # The description
    Time = requested_data['Time']                                               # Task's time
    try:
        itemsDict.append({'SN': SN, 'Disc': Disc, 'Time': Time})                # Append the new item to the list
        stored_data = {"SN": SN, "Disc": Disc, "Time": Time}                    # Store the new item
    except Exception as e:
        print('Error: ', e)
        stored_data = None                                                      # Set soted_data to None if there's an error
    if stored_data is None:
        response = Response("{'Error': 'The item was not created - " + item + "'}", status=400 , mimetype='application/json')     # Return error if item was not added (Bad request)
        return response
    response = Response(json.dumps(stored_data), mimetype='application/json')
    return response                                                             # Return response


#2: Update an existing item.
@app.route('/item/update', methods=['POST'])
def update_item():
    requested_data = request.get_json()                                         # Get the item's serial number from POST payload
    SN_New = requested_data['SN']                                               # The serial number
    Disc_New = requested_data['Disc']                                           # The description
    Time_New = requested_data['Time']                                           # Task's time
    try:
        for i in itemsDict:
            if i['SN'] == SN_New:
                i.update({"SN": SN_New, "Disc": Disc_New, "Time": Time_New})    # Update the target item with the new values
        stored_data = {"SN": SN_New, "Disc": Disc_New, "Time": Time_New}        # Store the new item
    except Exception as e:
        print('Error: ', e)
        stored_data = None
    if stored_data is None:                                                     # Set soted_data to None if there's an error
        response = Response("{'Error': 'The item was not created - " + item + "'}", status=400 , mimetype='application/json')
        return response
    response = Response(json.dumps(stored_data), mimetype='application/json')
    return response                                                             # Return response


#3: Delete an item.
@app.route('/item/delete', methods=['POST'])
def delete_item():
    requested_data = request.get_json()                                         # Get the item's serial number from POST payload
    SN_Del = requested_data['SN']                                               # The serial number
    try:
        j = 0
        for i in itemsDict:
            if i['SN'] == SN_Del:
                stored_data = {"SN": i['SN'], "Disc": i['Disc'], "Time": i['Time']} # Store the target item
                del itemsDict[j]                                                # Delete the target item
            j = j + 1
    except Exception as e:
        print('Error: ', e)
        stored_data = None                                                          # Set soted_data to None if there's an error
    if stored_data is None:
        response = Response("{'Error': 'The item was not created - " + item + "'}", status=400 , mimetype='application/json')
        return response
    response = Response(json.dumps(stored_data), mimetype='application/json')
    return response                                                             # Return response


#4: Get a particular item.
@app.route('/item/get', methods=['POST'])
def get_item():
    requested_data = request.get_json()                                         # Get the item's serial number from POST payload
    SN = requested_data['SN']                                                   # The serial number
    try:
        for i in itemsDict:
            if i['SN'] == SN:
                stored_data = {"SN": i['SN'], "Disc": i['Disc'], "Time": i['Time']} # Store the target item
    except Exception as e:
        print('Error: ', e)
        stored_data = None                                                      # Set soted_data to None if there's an error
    if stored_data is None:
        response = Response("{'Error': 'The item was not created - " + item + "'}", status=400 , mimetype='application/json')
        return response
    response = Response(json.dumps(stored_data), mimetype='application/json')
    return response                                                             # Return response


#5: Get all items from the list.
@app.route('/items/all')
def get_all_items():
    try:
        stored_data = itemsDict                                                 # Will return all items
    except Exception as e:
        print('Error: ', e)
        stored_data = None                                                      # Set soted_data to None if there's an error
    if stored_data is None:
        response = Response("{'Error': 'The item was not created - " + item + "'}", status=400 , mimetype='application/json')
        return response
    response = Response(json.dumps(stored_data), mimetype='application/json')
    return response
